import 'dotenv/config'
import { natsWrapper } from './nats_wrapper';
import { OrderCreatedListener } from './events/listeners/order_created_listener';

const start = async () => {
    if (!process.env.NATS_CLUSTER_ID) {
        throw new Error("No NATS CLUSTER ID present in env!");
    }
    if (!process.env.NATS_URL) {
        throw new Error("No NATS URL present in env!");
    }

    try {
        await natsWrapper.connect(process.env.NATS_CLUSTER_ID, 'expiration_client', process.env.NATS_URL);
        natsWrapper.client.on('close', () => {
            console.log('NATS disconnected');
            process.exit();
        })

        new OrderCreatedListener(natsWrapper.client).listen();

        process.on('SIGINT', () => { natsWrapper.client.close(); })
        process.on('SIGTERM', () => { natsWrapper.client.close(); })
    } catch (err) {
        console.error(err);
    }
};

start();
