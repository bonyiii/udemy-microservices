import { Publisher, ExpirationCompleteEvent, Subjects } from "@boni_udemy/common";

export class ExpirationCompletePublisher extends Publisher<ExpirationCompleteEvent> {
    subject: Subjects.ExpirationComplete = Subjects.ExpirationComplete;
}
