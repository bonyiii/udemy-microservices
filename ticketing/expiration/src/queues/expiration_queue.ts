import Queue from 'bull';
import { ExpirationCompletePublisher } from '../events/publishers/expiration_complete_publisher';
import { natsWrapper } from '../nats_wrapper';

interface Payload {
    orderId: string;
}

const expirationQueue = new Queue<Payload>('order-expiration', {
    redis: {
        host: 'localhost'
    }
});

expirationQueue.process(async (job) => {
    new ExpirationCompletePublisher(natsWrapper.client).publish({
        orderId: job.data.orderId
    });
    console.log('I want to publish an expiration:complete event for orderId', job.data.orderId);
});

export { expirationQueue };
