export enum OrderStatus {
    // Whe the order has been created, but the
    // ticket is trying to order has not been reserved
    Created = 'created',

    // The ticket the order is trying to reserved has already
    // been reserved or when the user has cancelled the order
    // The order expires before payment
    Cancelled = 'cancelled',

    // The order has successfuly reserved the ticket
    AwaitinPayment = 'awaiting:payment',

    // The order has reserved and the payment is done
    Complete = 'complete'
}
