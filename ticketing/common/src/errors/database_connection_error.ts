import { CustomError } from './custom_error'
export class DatabaseConnectionError extends CustomError {
    reason = 'Cannot connect to database';
    statusCode = 500;
    constructor() {
        super('DB error');

        // Only because we are extending a built in class
        Object.setPrototypeOf(this, DatabaseConnectionError.prototype);
    }

    serializeErrors() {
        return [
            { message: this.reason }
        ]

    }
}
