import { Publisher, OrderCancelledEvent, Subjects } from "@boni_udemy/common";

export class OrderCancelledPublisher extends Publisher<OrderCancelledEvent> {
    subject: Subjects.OrderCancelled = Subjects.OrderCancelled;
}

