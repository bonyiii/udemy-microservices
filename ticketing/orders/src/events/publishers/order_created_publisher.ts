import { Publisher, OrderCreatedEvent, Subjects } from "@boni_udemy/common";

export class OrderCreatedPublisher extends Publisher<OrderCreatedEvent> {
    subject: Subjects.OrderCreated = Subjects.OrderCreated;
}

