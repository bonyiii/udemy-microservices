import { Message } from 'node-nats-streaming';
import mongoose from "mongoose";
import { TicketUpdatedEvent } from '@boni_udemy/common';
import { natsWrapper } from "../../nats_wrapper";
import { Ticket } from '../../models/ticket';
import { TicketUpdatedListener } from './ticket_updated_listener';

const setup = async () => {
   // create an instance of listener
    const listener = new TicketUpdatedListener(natsWrapper.client);

    // Create and save ticket
    const ticket = Ticket.build({
        id: String(new mongoose.Types.ObjectId()),
        title: 'concert',
        price: 20
    });

    await ticket.save();

    // create a fake data object
    const data: TicketUpdatedEvent['data'] = {
        version: ticket.version + 1,
        id: ticket.id,
        title: 'updated concert',
        price: 10,
        userId: 'blah'
    }

    // create a fake message
    // @ts-ignore
    const msg: Message = {
        ack: jest.fn()
    }

    return { listener, data, msg, ticket }
}

it('finds, updates and saves a ticket', async () => {
    const { listener, data, msg, ticket } = await setup();
    // call the onMessage function with the data object + message object
    await listener.onMessage(data, msg)

    // write assertions to make sure a ticket was created
    const updateTicket = await Ticket.findById(ticket.id);

    expect(updateTicket!).toBeDefined();
    expect(updateTicket!.title).toEqual(data.title);
    expect(updateTicket!.price).toEqual(data.price);
})

it('acks the message', async () => {
    const { listener, data, msg } = await setup();
    // call the onMessage function with the data object + message object
    await listener.onMessage(data, msg)

    // make sure ack is called
    expect(msg.ack).toHaveBeenCalled()
})

it('handles out of order events', async () => {
    const { listener, data, msg } = await setup();
    data.version = 10;
    // call the onMessage function with the data object + message object
    try {
        await listener.onMessage(data, msg)
    }catch (err) {
    }

    expect(msg.ack).not.toHaveBeenCalled()
})
