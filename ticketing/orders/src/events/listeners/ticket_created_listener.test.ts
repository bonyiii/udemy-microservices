import { Message } from 'node-nats-streaming';
import mongoose from "mongoose";
import { TicketCreatedEvent } from '@boni_udemy/common';
import { TicketCreatedListener } from "./ticket_created_listener"
import { natsWrapper } from "../../nats_wrapper";
import { Ticket } from '../../models/ticket';

const setup = async () => {
   // create an instance of listener
    const listener = new TicketCreatedListener(natsWrapper.client);
    // create a fake data events
    const data: TicketCreatedEvent['data'] = {
        version: 0,
        id: String(new mongoose.Types.ObjectId()),
        title: 'concert',
        price: 10,
        userId: String(new mongoose.Types.ObjectId())

    }
    // create a fake message
    // @ts-ignore
    const msg: Message = {
        ack: jest.fn()
    }

    return { listener, data, msg }
}

it('creates and save a ticket', async () => {
    const { listener, data, msg } = await setup();
    // call the onMessage function with the data object + message object
    await listener.onMessage(data, msg)

    // write assertions to make sure a ticket was created
    const ticket = await Ticket.findById(data.id);

    expect(ticket).toBeDefined();
    expect(ticket!.title).toEqual(data.title);
    expect(ticket!.price).toEqual(data.price);
})

it('acks the message', async () => {
    const { listener, data, msg } = await setup();
    // call the onMessage function with the data object + message object
    await listener.onMessage(data, msg)

    // make sure ack is called
    expect(msg.ack).toHaveBeenCalled()
})
