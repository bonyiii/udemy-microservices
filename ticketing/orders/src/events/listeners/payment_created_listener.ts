import { Message } from 'node-nats-streaming';
import { Listener, PaymentCreatedEvent, Subjects, OrderStatus } from "@boni_udemy/common";
import { queueGroupName } from "./queue_group_name";
import { Order } from "../../models/order";

export class PaymentCreatedListener extends Listener<PaymentCreatedEvent> {
    subject: Subjects.PaymentCreated = Subjects.PaymentCreated;
    queueGroupName = queueGroupName

    async onMessage(data: PaymentCreatedEvent['data'], msg: Message) {
        const order = await Order.findById(data.orderId)

        if(!order) {
            throw new Error('Order not found')
        }


        // Because we expect no other service will use the order
        // after it gets completed we don't bother messaging
        // the new version number. Version number is incremented
        // on save by the mongoose-updateifcurrentplugin
        // Technically since this is the main service for orders
        // we should publish an event here when the order gets
        // updated.

        // Like we do in case of expirationcompletelistener
        order.set({
            status: OrderStatus.Complete
        })
        await order.save()

        msg.ack()
    }
}
