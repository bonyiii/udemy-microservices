import { ExpirationCompleteListener } from "./expiration_complete_listener";
import { natsWrapper } from "../../nats_wrapper";
import { Order, OrderStatus } from "../../models/order";
import { Ticket } from "../../models/ticket";
import mongoose from "mongoose";
import { ExpirationCompleteEvent } from "@boni_udemy/common";

const setup = async () => {
    const listener = new ExpirationCompleteListener(natsWrapper.client);
    const ticket = Ticket.build({
        id: String(new mongoose.Types.ObjectId()),
        title: 'concert',
        price: 20
    });
    await ticket.save();

    const order = Order.build({
        status: OrderStatus.Created,
        userId: 'blah',
        expiresAt: new Date(),
        ticket
    });
    await order.save();

    const data: ExpirationCompleteEvent['data'] = {
        orderId: order.id
    }

    // @ts-ignore
    const msg: Message = {
        ack: jest.fn()
    }

    return { listener, order, ticket, data, msg };
}

it('should update order status to cancelled', async () => {
    const { listener, order, ticket, data, msg } = await setup();
    await listener.onMessage(data, msg)
    const updatedOrder = await Order.findById(order.id);

    expect(updatedOrder!.status).toEqual(OrderStatus.Cancelled);
})

it('should publish an order cancelled event', async () => {
    const { listener, order, ticket, data, msg } = await setup();
    await listener.onMessage(data, msg)

    expect(natsWrapper.client.publish).toHaveBeenCalled()
    const eventData = JSON.parse((natsWrapper.client.publish as jest.Mock).mock.calls[0][1]);

    expect(eventData.id).toEqual(order.id);
})

it('ack the message', async () => {
    const { listener, order, ticket, data, msg } = await setup();
    await listener.onMessage(data, msg)

    expect(msg.ack).toHaveBeenCalled();
})
