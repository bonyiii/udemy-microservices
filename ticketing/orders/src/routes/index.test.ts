import request from 'supertest';
import { app } from '../app';
import { Order } from '../models/order';
import { Ticket } from '../models/ticket';
import mongoose from 'mongoose';

it("should return user orders", async () => {
    const ticket1 = Ticket.build({
        title: 'Concert',
        price: 20,
        id: String(new mongoose.Types.ObjectId())
    });

    await ticket1.save();

    const ticket2 = Ticket.build({
        title: 'Other Concert',
        price: 30,
        id: String(new mongoose.Types.ObjectId())
    });
    await ticket2.save();

    const ticket3 = Ticket.build({
        title: 'Festival',
        price: 10,
        id: String(new mongoose.Types.ObjectId())
    });
    await ticket3.save();

    const userOne = global.signin();
    const userTwo = global.signin();

    await request(app)
        .post('/api/orders')
        .set('Cookie', userOne)
        .send({ ticketId: ticket1.id })
        .expect(201)

    const { body: orderOne } = await request(app)
        .post('/api/orders')
        .set('Cookie', userTwo)
        .send({ ticketId: ticket2.id })
        .expect(201)

    const { body: orderTwo } = await request(app)
        .post('/api/orders')
        .set('Cookie', userTwo)
        .send({ ticketId: ticket3.id })
        .expect(201)


    const response = await request(app)
        .get('/api/orders')
        .set('Cookie', userTwo)
        .expect(200)

    expect(response.body.length).toEqual(2);
    expect(response.body[0].id).toEqual(orderOne.id);
    expect(response.body[1].id).toEqual(orderTwo.id);
    expect(response.body[0].ticket.id).toEqual(ticket2.id);
    expect(response.body[1].ticket.id).toEqual(ticket3.id);
});
