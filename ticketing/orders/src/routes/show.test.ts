import request from 'supertest';
import { app } from '../app';
import { Order } from '../models/order';
import { Ticket } from '../models/ticket';
import mongoose from 'mongoose';

it("fetches the order", async () => {
    const ticket = Ticket.build({
        title: 'Concert',
        price: 20,
        id: String(new mongoose.Types.ObjectId())
    });
    await ticket.save();

    const userOne = global.signin();

    const { body: orderOne } = await request(app)
        .post('/api/orders')
        .set('Cookie', userOne)
        .send({ ticketId: ticket.id })
        .expect(201)

    const response = await request(app)
        .get(`/api/orders/${orderOne.id}`)
        .set('Cookie', userOne)
        .send()
        .expect(200)

    expect(response.body.id).toEqual(orderOne.id);
});

it("returns not authorized error for other user order", async () => {
    const ticket = Ticket.build({
        title: 'Concert',
        price: 20,
        id: String(new mongoose.Types.ObjectId())
    });
    await ticket.save();

    const userOne = global.signin();
    const userTwo = global.signin();

    const { body: orderOne } = await request(app)
        .post('/api/orders')
        .set('Cookie', userOne)
        .send({ ticketId: ticket.id })
        .expect(201)

    await request(app)
        .get(`/api/orders/${orderOne.id}`)
        .set('Cookie', userTwo)
        .send()
        .expect(401)
});
