import request from "supertest"
import { app } from "../app"
import mongoose from "mongoose"
import { Order, OrderStatus } from "../models/order";
import { Ticket } from "../models/ticket";
import { natsWrapper } from "../nats_wrapper";

it("returns an error if the ticket does not exists", async () => {
    const ticketId = new mongoose.Types.ObjectId();

    await request(app)
        .post('/api/orders')
        .set('Cookie', global.signin())
        .send({ ticketId })
        .expect(404);
})

it("returns an error if the ticket is already reserved", async () => {
    const ticket = Ticket.build({
        title: 'Concert',
        price: 25,
        id: String(new mongoose.Types.ObjectId())
    })
    await ticket.save()

    const order = Order.build({
        ticket,
        userId: '2323',
        status: OrderStatus.Created,
        expiresAt: new Date()
    })
    await order.save();

    await request(app)
        .post('/api/orders')
        .set('Cookie', global.signin())
        .send({ ticketId: ticket.id })
        .expect(400);
})

it("returns a ticket", async () => {
    const ticket = Ticket.build({
        title: 'Concert',
        price: 25,
        id: String(new mongoose.Types.ObjectId())
    })
    await ticket.save()

    await request(app)
        .post('/api/orders')
        .set('Cookie', global.signin())
        .send({ ticketId: ticket.id })
        .expect(201);
})


it("emits an order created event", async () => {
    const ticket = Ticket.build({
        title: 'Concert',
        price: 25,
        id: String(new mongoose.Types.ObjectId())
    })
    await ticket.save()

    await request(app)
        .post('/api/orders')
        .set('Cookie', global.signin())
        .send({ ticketId: ticket.id })
        .expect(201);

    expect(natsWrapper.client.publish).toHaveBeenCalled();
});
