import request from 'supertest';
import { app } from '../app';
import { Order, OrderStatus } from '../models/order';
import { Ticket } from '../models/ticket';
import { natsWrapper } from '../nats_wrapper';
import mongoose from 'mongoose';

it("delete the order", async () => {
    const ticket = Ticket.build({
        title: 'Concert',
        price: 20,
        id: String(new mongoose.Types.ObjectId())
    });
    await ticket.save();

    const userOne = global.signin();

    const { body: orderOne } = await request(app)
        .post('/api/orders')
        .set('Cookie', userOne)
        .send({ ticketId: ticket.id })
        .expect(201)

    const response = await request(app)
        .delete(`/api/orders/${orderOne.id}`)
        .set('Cookie', userOne)
        .send()
        .expect(204)

    const updatedOrder = await Order.findById(orderOne.id);

    expect(updatedOrder!.status).toEqual(OrderStatus.Cancelled);
});

it('emits an order cancelled event', async () => {
    const ticket = Ticket.build({
        title: 'Concert',
        price: 20,
        id: String(new mongoose.Types.ObjectId())
    });
    await ticket.save();

    const userOne = global.signin();

    const { body: orderOne } = await request(app)
        .post('/api/orders')
        .set('Cookie', userOne)
        .send({ ticketId: ticket.id })
        .expect(201)

    const response = await request(app)
        .delete(`/api/orders/${orderOne.id}`)
        .set('Cookie', userOne)
        .send()
        .expect(204)

    expect(natsWrapper.client.publish).toHaveBeenCalled();
});
