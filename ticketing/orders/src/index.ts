import mongoose from 'mongoose';
import { app } from './app';
import { natsWrapper } from './nats_wrapper';
import { TicketCreatedListener,  } from './events/listeners/ticket_created_listener';
import { TicketUpdatedListener } from './events/listeners/ticket_updated_listener';
import { ExpirationCompleteListener } from './events/listeners/expiration_complete_listener';
import { PaymentCreatedListener } from './events/listeners/payment_created_listener';

const start = async () => {
    if (!process.env.JWT_KEY) {
	throw new Error("No JWT KEY present in env!");
    }
    if (!process.env.NATS_CLUSTER_ID) {
	throw new Error("No JWT KEY present in env!");
    }
    if (!process.env.NATS_URL) {
	throw new Error("No JWT KEY present in env!");
    }

    try {
        await natsWrapper.connect(process.env.NATS_CLUSTER_ID, 'orders_client', process.env.NATS_URL);
        natsWrapper.client.on('close', () => {
            console.log('NATS disconnected');
            process.exit();
        })
        natsWrapper.client.on('SIGINT', () => { natsWrapper.client.close(); })
        natsWrapper.client.on('SIGTERM', () => { natsWrapper.client.close(); })

        new TicketCreatedListener(natsWrapper.client).listen();
        new TicketUpdatedListener(natsWrapper.client).listen();
        new ExpirationCompleteListener(natsWrapper.client).listen();
        new PaymentCreatedListener(natsWrapper.client).listen();

        await mongoose.connect('mongodb://192.168.15.100:27017/orders');
        console.log('Connected to mongodb');
    } catch (err) {
        console.error(err);
    }

    app.listen(8083, () => {
        console.log('Listening on port 8083!');
    });
};

start();
