import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';

declare global {
    var signin: (id?: string) => string[];
}

global.signin = (id?: string) => {
    const payload = {
        id: id || new mongoose.Types.ObjectId(),
        email: "test@test.com"
    };

    // Create JWT
    const token = jwt.sign(payload, process.env.JWT_KEY!);

    const session = { jwt: token };

    const sessionJSON = JSON.stringify(session);

    const base64 = Buffer.from(sessionJSON).toString("base64");

    return [`session=${base64}`];
}

let mongo: any;
jest.mock('../nats_wrapper');

beforeAll(async () => {
    process.env.JWT_KEY = 'asdfasdf';
    //mongo = new MongoMemoryServer();
    mongo = await MongoMemoryServer.create();
    const mongoUri = await mongo.getUri();

    await mongoose.connect(mongoUri, {
        //        useNewUrlParser: true,
        //        useUnifiedTopology: true
    });
});

beforeEach(async () => {
    jest.clearAllMocks();
    const collections = await mongoose.connection.db.collections();

    for (let collection of collections) {
        await collection.deleteMany({});
    }
});


afterAll(async () => {
    await mongo.stop;
    await mongoose.connection.close();
})
