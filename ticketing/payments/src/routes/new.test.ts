import request from 'supertest'
import mongoose from 'mongoose'
import { app } from '../app'
import { Order } from '../models/order';
import { OrderStatus } from '@boni_udemy/common';
import { stripe } from '../stripe';
import { Payment } from '../models/payment';

// Mocked stripe
// In order to use it rename stripe.ts.old back to stripe.ts in the __mocks__ folder.
//jest.mock('../stripe.ts')

process.env.STRIPE_SECRET_KEY='sk_test_51L2VZID4CFHQnRd3fT4SYwzUNURrQrCv3Z9v7flV7U6XH5aAm9PTbeuExq0SSKFt8EsKEuzP4xsQJzpcOAbU1icZ00q9sMV2MQ';

it('return 404 when purchasing order which does not exists', async () => {
    await request(app)
        .post('/api/payments')
        .set('Cookie', global.signin())
        .send({
            token: 'adfs',
            orderId: new mongoose.Types.ObjectId()
        })
        .expect(404);
})

it('returns 401 when purchasing order which does not belong to the user', async () => {
    const order = Order.build({
        id: String(new mongoose.Types.ObjectId()),
        version: 0,
        userId: 'blah',
        price: 10,
        status: OrderStatus.Created
    })
    await order.save()

    await request(app)
        .post('/api/payments')
        .set('Cookie', global.signin())
        .send({
            token: 'adfs',
            orderId: order.id
        })
        .expect(401);
})

it('return a 400 when purchasing a cancelled order', async () => {
    const userId = String(new mongoose.Types.ObjectId())
    const order = Order.build({
        id: String(new mongoose.Types.ObjectId()),
        version: 0,
        userId: userId,
        price: 10,
        status: OrderStatus.Cancelled
    })
    await order.save()

    await request(app)
        .post('/api/payments')
        .set('Cookie', global.signin(userId))
        .send({
            token: 'adfs',
            orderId: order.id
        })
        .expect(400);
})

it('returns a 201 with valid inputs', async () => {
    const userId = String(new mongoose.Types.ObjectId())
    const price = Math.floor(Math.random() * 100000);
    const order = Order.build({
        id: String(new mongoose.Types.ObjectId()),
        version: 0,
        userId: userId,
        price,
        status: OrderStatus.Created
    })
    await order.save()

    await request(app)
        .post('/api/payments')
        .set('Cookie', global.signin(userId))
        .send({
            token: 'tok_visa',
            orderId: order.id
        })
        .expect(201);


    const stripeCharges = await stripe.charges.list({ limit: 50 });
    const stripeCharge = stripeCharges.data.find(charge => {
        return charge.amount === price * 100;
    })
    expect(stripeCharge).toBeDefined();
    expect(stripeCharge!.currency).toEqual('usd')

    const payment = await Payment.findOne({
        orderId: order.id,
        stripId: stripeCharge!.id
    })

    expect(payment).not.toBeNull();

    // Mocked stripe
    // test expectations
    // const chargeOptions = (stripe.charges.create as jest.Mock).mock.calls[0][0]
    // expect(chargeOptions.source).toEqual('tok_visa')
    // expect(chargeOptions.amount).toEqual(order.price * 100)
    // expect(chargeOptions.currency).toEqual('usd')
})
