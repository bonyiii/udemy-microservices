import mongoose from 'mongoose';
import { app } from './app';
import { natsWrapper } from './nats_wrapper';
import { OrderCreatedListener } from './events/listeners/order_created_listener';
import { OrderCancelledListener } from './events/listeners/order_cancelled_listener';

const start = async () => {
    if (!process.env.JWT_KEY) {
        throw new Error("No JWT KEY present in env!");
    }
    if (!process.env.NATS_CLUSTER_ID) {
        throw new Error("No JWT KEY present in env!");
    }
    if (!process.env.NATS_URL) {
        throw new Error("No JWT KEY present in env!");
    }

    try {
        await natsWrapper.connect(process.env.NATS_CLUSTER_ID, 'payments_client', process.env.NATS_URL);
        natsWrapper.client.on('close', () => {
            console.log('NATS disconnected');
            process.exit();
        })
        natsWrapper.client.on('SIGINT', () => { natsWrapper.client.close(); })
        natsWrapper.client.on('SIGTERM', () => { natsWrapper.client.close(); })

        new OrderCreatedListener(natsWrapper.client).listen();
        new OrderCancelledListener(natsWrapper.client).listen();

        await mongoose.connect('mongodb://192.168.15.100:27017/payments');
        console.log('Connected to mongodb');
    } catch (err) {
        console.error(err);
    }

    app.listen(8084, () => {
        console.log('Listening on port 8084!');
    });
};

start();
