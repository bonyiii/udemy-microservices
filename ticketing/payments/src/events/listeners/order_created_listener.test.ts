import { Message } from 'node-nats-streaming';
import { OrderCreatedListener } from "./order_created_listener"
import { natsWrapper } from "../../nats_wrapper"
import { OrderCreatedEvent, OrderStatus } from "@boni_udemy/common"
import mongoose from "mongoose"
import { Order } from "../../models/order"

const setup = async () => {
    const listener = new OrderCreatedListener(natsWrapper.client)

    const data: OrderCreatedEvent['data'] = {
        id: String(new mongoose.Types.ObjectId()),
        version: 0,
        expiresAt: 'blah',
        userId: 'dfasd',
        status: OrderStatus.Created,
        ticket: {
            id: 'dsfs',
            price: 10
        }
    };

    // @ts-ignore
    const msg: Message = {
        ack: jest.fn()
    }

    return { listener, data, msg }
}

it('replicates the order info', async () => {
    const { listener, data, msg } = await setup()
    await listener.onMessage(data, msg)
    const order = await Order.findById(data.id)

    expect(order!.id).toEqual(data.id);
})

it('acks the message', async () => {
    const { listener, data, msg } = await setup()
    await listener.onMessage(data, msg)

    expect(msg.ack).toHaveBeenCalled()
})
