import { Subjects, Publisher, PaymentCreatedEvent } from '@boni_udemy/common'

export class PaymentCreatedPublisher extends Publisher<PaymentCreatedEvent> {
    subject: Subjects.PaymentCreated = Subjects.PaymentCreated;
}
