curl -iL -H 'Content-Type: application/json' -d '{"email": "adfsdf", "password": "1"}' http://localhost:300/api/users/signup

curl -iL --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' -d '{"email": "adfsdf", "password": "1"}' http://localhost:8080/api/users/signup

curl -iL -v --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' -d '{"email": "bbf@sdf.hu", "password": "1234"}' http://localhost:8080/api/users/signup

curl -iLv --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' -d '{"email": "acm@sdf.hu", "password": "1234"}' http://localhost:8080/api/users/signin

curl -iLv --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' http://localhost:8080/api/users/current_user

curl -iLv --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' -d '{}' http://localhost:8080/api/users/signout

# Create ticket
curl -iLv --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' -d '{"title": "New Concerto", "price": 500 }' http://ticketing.dev/api/tickets


# Update ticket (figure out if -X can be left out)
curl -X PUT -iLv --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' -d '{"title": "New Concerto", "price": 502}' http://ticketing.dev/api/tickets/62737f23e2b1602db5260517

# Create Order
curl -iLv --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' -d '{"ticketId": "6284b12d0adfb900b6da0180"}' http://ticketing.dev/api/orders/


# Create Payment
curl -iLv --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' -d '{}' http://ticketing.dev/api/payments


# Create a Charge
curl -iLv --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' -d '{"title": "New Concerto", "price": 15 }' http://ticketing.dev/api/tickets
curl -iLv --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' -d '{"ticketId": ""}' http://ticketing.dev/api/orders/
curl -iLv --cookie-jar auth-cookie.jar -b auth-cookie.jar -H 'Content-Type: application/json' -d '{"token": "tok_visa", "orderId": ""}' http://ticketing.dev/api/payments
