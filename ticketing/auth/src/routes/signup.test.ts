import request from 'supertest';
import { app } from '../app';
import { contextsKey } from 'express-validator/src/base';

it('should return a 201 on successfule signup', async () =>  {
    return request(app)
        .post('/api/users/signup')
        .send({
            email: 'test@test.com',
            password: 'password'
        })
        .expect(201);
})

describe('with invalid params', () => {
    it('should return 400 with invalid email', async () => {
        return request(app)
            .post('/api/users/signup')
            .send({
                email: 'invalid email',
                password: 'password'
            })
            .expect(400);
    });

    it('should return 400 with invalid passwordbp', async () => {
        return request(app)
            .post('/api/users/signup')
            .send({
                email: 'val@id.email',
                password: ''
            })
            .expect(400);
    });

    it('should return 400 with empty params', async () => {
        return request(app)
            .post('/api/users/signup')
            .send({})
            .expect(400);
    });
});

describe('with duplicate emails',  () => {
    beforeEach(async () => {
        await request(app)
            .post('/api/users/signup')
            .send({
                email: 'test@test.com',
                password: 'password'
            })
            .expect(201);
    });

    it('disallows duplicate email', async () => {
        return request(app)
            .post('/api/users/signup')
            .send({
                email: 'test@test.com',
                password: 'password'
            })
            .expect(400);
    });
});

describe('headers', () => {
    let response: request.Response;
    beforeEach(async () => {
        response = await request(app)
            .post('/api/users/signup')
            .send({
                email: 'test@test.com',
                password: 'password'
            })
            .expect(201);
    });

    it('should set cookie', async () => {
        expect(response.get('Set-Cookie')).toBeDefined();
    })
});
