import request from 'supertest';
import { app } from '../app';

describe('with a user alredy signed up', () => {
    beforeEach(async () => {
        await request(app)
            .post('/api/users/signup')
            .send({
                email: 'test@test.com',
                password: 'password'
            })
            .expect(201);
    });

    it('should return a 200 on successfule signin', async () =>  {
        return request(app)
            .post('/api/users/signin')
            .send({
                email: 'test@test.com',
                password: 'password'
            })
            .expect(200);
    })

    it('should return a cookie', async () =>  {
        const response = await request(app)
            .post('/api/users/signin')
            .send({
                email: 'test@test.com',
                password: 'password'
            })
            .expect(200);

        expect(response.get('Set-Cookie')).toBeDefined();
    })

    it('should return a 400 on bad password', async () =>  {
        return request(app)
            .post('/api/users/signin')
            .send({
                email: 'test@test.com',
                password: 'bad'
            })
            .expect(400);
    })
})

describe('without an alredy signed up user', () => {
})
