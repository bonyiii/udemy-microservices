import express, { Request, Response } from 'express';
import { currentUser } from '@boni_udemy/common';

const router = express.Router();

router.get('/api/users/current_user',
    currentUser,
    (req: Request, res: Response) => {
        res.send({ currentUser: req.currentUser || null });
    });

export { router as currentUserRouter };
