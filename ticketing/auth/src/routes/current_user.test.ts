import request from 'supertest';
import { app } from '../app';

describe('with a user alredy signed up', () => {
    // How do I know cookie is string[]
    // I set cookie as const in beforeeach body
    // then moved the cursor over it and it displayed
    // string[] so I set it here as well. The reason
    // is that at this time only the variable is defined
    // so typescript cannot determine the type automatically.
    let cookie: string[];
    beforeEach(async () => {
        cookie = await global.signin()
    });

    it('should respond with details about the current user', async () => {
        const response = await request(app)
            .get('/api/users/current_user')
            .set('Cookie', cookie)
            .send()
            .expect(200);

        expect(response.body.currentUser.email).toEqual('test@test.com')
        //console.log(response.body);
    });
});
describe('with a user alredy signed up', () => {
    it('should respond with null if not authenticated', async () => {
        const response = await request(app)
            .get('/api/users/current_user')
            .send()
            .expect(200);

        expect(response.body.currentUser).toBe(null);
    });
});
