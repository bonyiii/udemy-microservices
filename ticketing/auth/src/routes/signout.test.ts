import request from 'supertest';
import { app } from '../app';

describe('with a user alredy signed up', () => {
    beforeEach(async () => {
        await request(app)
            .post('/api/users/signup')
            .send({
                email: 'test@test.com',
                password: 'password'
            })
            .expect(201);
    });

    it('should return a 200 on successfule sigout', async () =>  {
        const response = await request(app)
            .post('/api/users/signout')
            .send({})
            .expect(200);

        expect(response.get('Set-Cookie')).toBeDefined();
    });
});
