import mongoose from 'mongoose';
import { Password } from '../services/password'

// Define typscript types in an interface
// buildUser
interface UserAttrs {
    email: string;
    password: string;
}

// Interface that describe user model attributes
interface UserModel extends mongoose.Model<UserDoc> {
    build(attrs: UserAttrs): UserDoc;
}

// An interface that describes the user Document
interface UserDoc extends mongoose.Document {
    email: string;
    password: string;
}

// Mongoose schema definition (using mongo types not Typscript types)
const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
}, {
    toJSON: {
        transform(doc, ret) {
            ret.id = ret._id;
            delete ret._id;
            delete ret.password;
            delete ret.__v;
        }
    }
});
userSchema.statics.build = (attrs: UserAttrs) => {
    return new User(attrs);
}

userSchema.pre('save', async function(done) {
    if (this.isModified('password')) {
        const hashed = await Password.toHash(this.get('password'));
        this.set('password', hashed);
    }
    done();
});

const User = mongoose.model<UserDoc, UserModel>('User', userSchema);

export { User };

