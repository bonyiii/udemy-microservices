import mongoose from 'mongoose';
import { app } from './app';

const start = async () => {
    if (!process.env.JWT_KEY) {
	throw new Error("No JWT KEY present in env!");
    }

    try {
        await mongoose.connect('mongodb://192.168.15.100:27017/auth');
        console.log('Connected to mongodb');
    } catch (err) {
        console.error(err);
    }
    app.listen(8080, () => {
        console.log('Listening on port 8080!');
    });
};

start();
