import { Ticket } from "./ticket";

it('implements optimistic concurrency control', async () => {
    const ticket = Ticket.build({
        title: 'Blah',
        price: 10,
        userId: 'blah'
    });

    await ticket.save();

    const ticket1 = await Ticket.findById(ticket.id)
    const ticket2 = await Ticket.findById(ticket.id)

    ticket1!.set({ title: 'Concert' })
    ticket2!.set({ title: 'Festival' })

    await ticket1!.save();
    try {
        await ticket2!.save();
    } catch (err) {
        return;
    }

    throw new Error('Test should not reach this point');
})


it('increments the version number on multiple saves', async () => {
    const ticket = Ticket.build({
        title: 'concecrto',
        price: 20,
        userId: '123'
    })

    await ticket.save();
    expect(ticket.version).toEqual(0);
    await ticket.save();
    expect(ticket.version).toEqual(1);
    await ticket.save();
    expect(ticket.version).toEqual(2);
})
