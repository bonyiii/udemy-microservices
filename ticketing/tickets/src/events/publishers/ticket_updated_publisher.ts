import { Publisher, Subjects, TicketUpdatedEvent } from "@boni_udemy/common";

export class TicketUpdatedPublisher extends Publisher<TicketUpdatedEvent> {
    readonly subject: Subjects.TicketUpdated = Subjects.TicketUpdated;
}
