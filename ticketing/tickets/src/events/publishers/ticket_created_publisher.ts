import { Publisher, Subjects, TicketCreatedEvent } from "@boni_udemy/common";

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent> {
    readonly subject: Subjects.TicketCreated = Subjects.TicketCreated;
}
