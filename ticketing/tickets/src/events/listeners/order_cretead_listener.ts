import { Message } from 'node-nats-streaming';
import { Subjects, Listener, OrderCreatedEvent, NotFoundError } from '@boni_udemy/common';
import { Ticket } from '../../models/ticket';
import { queueGroupName } from './queue_group_name';
import { TicketUpdatedPublisher } from '../publishers/ticket_updated_publisher';

export class OrderCreatedListener extends Listener<OrderCreatedEvent> {
    subject: Subjects.OrderCreated = Subjects.OrderCreated;
    queueGroupName = queueGroupName;

    async onMessage(data: OrderCreatedEvent['data'], msg: Message) {
        // find ticket
        const ticket = await Ticket.findById(data.ticket.id)

        // if no ticket, throw error
        if (!ticket) {
            throw new Error('Ticket not found');
        }

        // mark ticket as being reserved by setting orderId
        ticket.set({ orderId: data.id });

        // save the ticket
        await ticket.save()

        // await makes sure ack is only sent if this passes
        await new TicketUpdatedPublisher(this.client).publish({
            id: ticket.id,
            price: ticket.price,
            title: ticket.title,
            userId: ticket.userId,
            orderId: ticket.orderId,
            version: ticket.version
        });

        // ack the message
        msg.ack();
    }
}
