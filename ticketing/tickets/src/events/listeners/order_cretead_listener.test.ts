import { Message } from 'node-nats-streaming';
import { natsWrapper } from "../../nats_wrapper"
import { OrderCreatedListener } from "./order_cretead_listener"
import { Ticket } from "../../models/ticket";
import { OrderCreatedEvent, OrderStatus } from "@boni_udemy/common";
import mongoose from "mongoose";

const setup = async () => {
    // Create an instance of the listener
    const listener = new OrderCreatedListener(natsWrapper.client)

    // Create and save a ticket
    const ticket = Ticket.build({
        title: 'concert',
        price: 99,
        userId: 'asdf'
    });

    await ticket.save()

    // create the fake data event
    const data: OrderCreatedEvent['data'] = {
        id: String(new mongoose.Types.ObjectId()),
        version: 0,
        status: OrderStatus.Created,
        userId: 'asdf',
        expiresAt: 'blah',
        ticket: {
            id: ticket.id,
            price: ticket.price
        }
    }

    // @ts-ignore
    const msg: Message = {
        ack: jest.fn()
    }

    return { listener, ticket, data, msg };
}

it('sets the userId of the ticket', async () => {
    const { listener, ticket, data, msg } = await setup();
    await listener.onMessage(data, msg);
    const updatedTicket = await Ticket.findById(ticket.id);

    expect(updatedTicket!.orderId).toEqual(data.id);
 });

it('acks the message', async () => {
    const { listener, ticket, data, msg } = await setup();
    await listener.onMessage(data, msg);
    await Ticket.findById(ticket.id);

    expect(msg.ack).toHaveBeenCalled();
});

it('emits a ticket update publisher', async () => {
    const { listener, ticket, data, msg } = await setup();
    await listener.onMessage(data, msg);

    expect(natsWrapper.client.publish).toHaveBeenCalledTimes(1);
    // @ts-ignore
    console.log(natsWrapper.client.publish.mock.calls)

    const ticketUpdatedData = JSON.parse((natsWrapper.client.publish as jest.Mock).mock.calls[0][1])
    expect(data.id).toEqual(ticketUpdatedData.orderId)
});
