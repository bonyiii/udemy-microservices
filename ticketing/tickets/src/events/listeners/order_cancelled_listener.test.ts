import { Message } from 'node-nats-streaming';
import { natsWrapper } from "../../nats_wrapper"
import { Ticket } from "../../models/ticket";
import { OrderCancelledEvent } from "@boni_udemy/common";
import mongoose from "mongoose";
import { OrderCancelledListener } from './order_cancelled_listener';

const setup = async () => {
    // Create an instance of the listener
    const listener = new OrderCancelledListener(natsWrapper.client)

    // Create and save a ticket
    const ticket = Ticket.build({
        title: 'concert',
        price: 99,
        userId: 'asdf',

    });

    const orderId = String(new mongoose.Types.ObjectId());
    ticket.set({ orderId });

    await ticket.save()

    // create the fake data event
    const data: OrderCancelledEvent['data'] = {
        id: orderId,
        version: 0,
        ticket: {
            id: ticket.id,
        }
    }

    // @ts-ignore
    const msg: Message = {
        ack: jest.fn()
    }

    return { listener, ticket, data, msg, orderId };
}

it('sets the userId of the ticket', async () => {
    const { listener, ticket, data, msg } = await setup();
    await listener.onMessage(data, msg);
    const updatedTicket = await Ticket.findById(ticket.id);

    expect(updatedTicket!.orderId).toEqual(undefined);
    expect(updatedTicket!.orderId).not.toBeDefined()
    expect(msg.ack).toHaveBeenCalled();
 });

it('emits a ticket update publisher', async () => {
    const { listener, data, msg } = await setup();
    await listener.onMessage(data, msg);

    expect(natsWrapper.client.publish).toHaveBeenCalledTimes(1);
    // @ts-ignore
    console.log(natsWrapper.client.publish.mock.calls)

    const ticketUpdatedData = JSON.parse((natsWrapper.client.publish as jest.Mock).mock.calls[0][1])
    expect(ticketUpdatedData.orderId).toEqual(undefined)
});
