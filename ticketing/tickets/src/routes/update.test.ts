import request from 'supertest';
import { app } from '../app';
import mongoose from 'mongoose';
import { natsWrapper } from '../nats_wrapper';
import { Ticket } from '../models/ticket';

it('returns 404 if the ticket is not exists', async () => {
    const id = new mongoose.Types.ObjectId();

    await request(app)
        .put(`/api/tickets/${id}`)
        .set('Cookie', global.signin())
        .send({
            title: 'concert',
            price: 20
        })
        .expect(404);
});

it('returns 401 if the user is not authenticated', async () => {
    const id = new mongoose.Types.ObjectId();

    await request(app)
        .put(`/api/tickets/${id}`)
        .send()
        .expect(401);
});

it('returns 401 if the user is does not own the ticket', async () => {
    const response = await request(app)
        .post('/api/tickets')
        .set('Cookie', global.signin())
        .send({
            title: 'concert',
            price: 20
        });

    await request(app)
        .put(`/api/tickets/${response.body.id}`)
        .set('Cookie', global.signin())
        .send({
            title: 'concert',
            price: 21
        })
        .expect(401);
});

it('returns 400 if the user provides an invalid title or price', async () => {
    const cookie = global.signin();
    const response = await request(app)
        .post('/api/tickets')
        .set('Cookie', cookie)
        .send({
            title: 'concert',
            price: 20
        });

    await request(app)
        .put(`/api/tickets/${response.body.id}`)
        .set('Cookie', cookie)
        .send({
            title: '',
            price: 21
        })
        .expect(400);

    await request(app)
        .put(`/api/tickets/${response.body.id}`)
        .set('Cookie', cookie)
        .send({
            title: 'fdisofjaso',
            price: -1
        })
        .expect(400);
})

it('update tickets', async () => {
    const cookie = global.signin();
    const response = await request(app)
        .post('/api/tickets')
        .set('Cookie', cookie)
        .send({
            title: 'concert',
            price: 20
        });

    await request(app)
        .put(`/api/tickets/${response.body.id}`)
        .set('Cookie', cookie)
        .send({
            title: 'concerto',
            price: 21
        })
        .expect(200);

    const ticketResponse = await request(app)
        .get(`/api/tickets/${response.body.id}`)
        .send()

    expect(ticketResponse.body.title).toEqual('concerto');
    expect(ticketResponse.body.price).toEqual(21);
})

it('publishes an event', async () => {
    const cookie = global.signin();
    const response = await request(app)
        .post('/api/tickets')
        .set('Cookie', cookie)
        .send({
            title: 'concert',
            price: 20
        });

    await request(app)
        .put(`/api/tickets/${response.body.id}`)
        .set('Cookie', cookie)
        .send({
            title: 'concerto',
            price: 21
        })
        .expect(200);

    expect(natsWrapper.client.publish).toHaveBeenCalled();
})

it('rejects update if ticket is reserved', async () => {
  const cookie = global.signin();
    const response = await request(app)
        .post('/api/tickets')
        .set('Cookie', cookie)
        .send({
            title: 'concert',
            price: 20
        });

    const ticket = await Ticket.findById(response.body.id);
    ticket!.set({ orderId: String(new mongoose.Types.ObjectId()) })
    await ticket!.save()

    await request(app)
        .put(`/api/tickets/${response.body.id}`)
        .set('Cookie', cookie)
        .send({
            title: 'concerto',
            price: 21
        })
        .expect(400);
})
