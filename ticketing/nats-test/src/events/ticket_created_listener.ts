import { Message }from 'node-nats-streaming';
import { TicketCreatedEvent, Listener, Subjects } from '@boni_udemy/common';

class TicketCreatedListener extends Listener<TicketCreatedEvent> {
    readonly subject: Subjects.TicketCreated = Subjects.TicketCreated;
    queueGroupName = 'payments-service';

    onMessage(data: TicketCreatedEvent['data'] , msg: Message) {
        console.log('Event data', data);
        console.log('Event data', data.title);
        console.log('Event data', data.price);

        msg.ack();
    }
}

export { TicketCreatedListener };
