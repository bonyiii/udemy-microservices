import useRequest from '../../hooks/use_request'
import { useRouter } from 'next/router'

const TicketShow = ({ ticket }) => {
  const router = useRouter()
  const { doRequest, errors } = useRequest({
    url: '/api/orders',
    method: 'post',
    body: {
      ticketId: ticket.id
    },
    onSuccess: (order) => router.push({ pathname: '/orders/[orderId]', query: { orderId: order.id} })
  })

  return (<div>
    <div>
    <h4>{ticket.title}</h4>
    <h4>Price: {ticket.price}</h4>
    {errors}
    <button className='btn btn-primary' onClick={(event) => doRequest()}>Purchase</button>
    </div>
  </div>)
}

TicketShow.getInitialProps = async (context, client) => {
  const { ticketId } = context.query;
  const { data } = await client.get(`/api/tickets/${ticketId}`)

  return { ticket: data }
}

export default TicketShow;
