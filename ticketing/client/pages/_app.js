import 'bootstrap/dist/css/bootstrap.css';
import buildClient from '../api/build_client';
import Header from '../components/header'

const AppWrapper = ({ Component, pageProps, currentUser }) => {
  return(
    <div>
    <Header currentUser={currentUser} />
    <div className='container'>
    <Component {...pageProps} currentUser={currentUser} />
    </div>
    </div>
  );
}

AppWrapper.getInitialProps = async (appContext) => {
  const apiClient = buildClient(appContext.ctx);
  const { data } = await apiClient.get('/api/users/current_user').catch((err) => {
    console.log(err.message);
  });

  let pageProps = {}
  if (appContext.Component.getInitialProps) {
    pageProps = await appContext.Component.getInitialProps(appContext.ctx, apiClient, data.currentUser);
  }

  return {
    pageProps,
    ...data
  }
};


export default AppWrapper
