import { useEffect, useState } from 'react';
import { useRouter } from 'next/router'
import StripeCheckout from 'react-stripe-checkout';
import useRequest from '../../hooks/use_request'

const OrderShow = ({order, currentUser}) => {
  const router = useRouter()
  const [timeLeft, setTimeLeft] = useState(0)

  const { doRequest, errors } = useRequest({
    url: '/api/payments',
    method: 'post',
    body: {
      orderId: order.id
    },
    onSuccess: (payment) => router.push('/orders')
  })


  useEffect(() => {
    const findTimeLeft = () => {
      const msLeft = new Date(order.expiresAt) - new Date();
      setTimeLeft(Math.round(msLeft / 1000));
    }
    findTimeLeft();
    const timer = setInterval(findTimeLeft, 1000);

    return () => {
      clearInterval(timer);
    };
  }, [order])

  if (timeLeft < 0) {
    return <div>Order expired</div>
  }

  return (
    <div>
    Time left to pay {timeLeft} seconds
    <StripeCheckout
    token = {({ id }) => doRequest({ token: id })}
    stripeKey="pk_test_51L2VZID4CFHQnRd3S45fvXsJyfWnLkkefL7OHRf9rGHXvHC8EgATv2vLUbCKn6EjcoqdIvMZQYsIXZh2UBFgDEXo00RdBqquEd"
    amount={order.ticket.price * 100}
    email={currentUser.email}
    />
    {errors}
    </div>
  )
}

OrderShow.getInitialProps = async (context, client) => {
  const { orderId } = context.query
  const { data } = await client.get(`/api/orders/${orderId}`)

  return { order: data }
}

export default OrderShow
