import axios from 'axios';

const buildClient = ({ req }) => {
  if (typeof window === 'undefined') {
    // on the server side
    // console.log('buildClient on the server side');

    return axios.create({
      baseUrl: `http://${req.headers.host}`,
      headers: req.headers
    });
  } else {
    // on the client side
    // console.log('buildClient on the client side');

    return axios.create({
      baseUrl: '/'
    });
  }
}

export default buildClient;
